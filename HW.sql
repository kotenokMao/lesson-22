-- 1. Найти самый дорогой товар. Вывести имя товара и его цену

select Prices.value, Goods.name 
from Prices join Goods on Prices.goods_id = Goods.id
where Prices.value = (select max(Prices.value) from Prices)

-- 2. Найти товары с нулевым остатком. Вывести имя товара и его цену

select Goods.name, Prices.value as price
from Prices join Goods on Prices.goods_id = Goods.id
where Prices.goods_id in (select Quantity.goods_id from Quantity
						where Quantity.value = 0)
						
-- 3. Найти производителя с самой большой средней ценой за товары. 
-- Вывести имя производителя и среднюю стоимость.

-- 1 вариант:
select m.name as name, avg(p.Value) as avgValue 
from Prices as p 
left join Goods as g on p.goods_id = g.id
left join Suppliers as s on g.supplier_id = s.id
left join Manufacturer as m on s.manufacturer_id = m.id
group by m.name
order by avgValue desc
limit 1

-- 2 вариант:
select res.name, res.value
from (
	select m.id as id, m.name as name, avg(p.Value) as value 
	from Prices as p 
	left join Goods as g on p.goods_id = g.id
	left join Suppliers as s on g.supplier_id = s.id
	left join Manufacturer as m on s.manufacturer_id = m.id
	group by m.id) as res
where res.value =
	(select max(r.value)
	from (
	select m.id as id, m.name as name, avg(p.Value) as value 
	from Prices as p 
	left join Goods as g on p.goods_id = g.id
	left join Suppliers as s on g.supplier_id = s.id
	left join Manufacturer as m on s.manufacturer_id = m.id
	group by m.id) as r)

-- 4. Найти все товары производителей из Москвы. Вывести имена товаров, их цены и имена производителей

select g.name as name, p.value as value, m.name as manufacturer, m.location as location
from Goods as g 
left join Prices as p on g.id =  p.goods_id
left join Suppliers as s on g.supplier_id = s.id
left join Manufacturer as m on s.manufacturer_id = m.id
where m.location =  'Moscow'

